﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace GDEVDAT_FINALS
{
    class Program
    {
        public static string connectionString = "Data Source=TAFT-CL528;Initial Catalog = MarasiganCliftonKyleTG001; Persist Security Info=True;User ID = sa; Password=benilde";


        static void AddUser(int userid, string username, string emailaddress)
        {
            using (SqlConnection con = new SqlConnection(
        connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "INSERT INTO USERDATABASE VALUES(@UserID, @Username, @EmailAddress)", con))
                    {
                        command.Parameters.Add(new SqlParameter("UserID", userid));
                        command.Parameters.Add(new SqlParameter("Username", username));
                        command.Parameters.Add(new SqlParameter("EmailAddress", emailaddress));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Data cannot be inserted, Please make sure all details are unique");
                }
            }
        }
        static void TryDisplayEmail(string p_username)
        {
            using (SqlConnection con = new SqlConnection(
        connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "SELECT * FROM USERDATABASE WHERE Username ='" + p_username + "'", con))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            string emailAddress = reader.GetString(2);

                            if (emailAddress !=null )
                            {
                                Console.WriteLine(emailAddress);
                            }
                            else
                            {
                                Console.WriteLine("Could not fetch data, Please make sure the data exists");
                            }
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Could not fetch data");
                }
            }
        }
        static void AddCharacter(string username, string characterName,int level, int strength, int agility, int intelligence)
        {
            using (SqlConnection con = new SqlConnection(
   connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "INSERT INTO Characters VALUES(@Username, @CharacterName, @CharacterStrength, @CharacterAgility, @CharacterIntelligence, @CharacterLevel)", con))
                    {
                        command.Parameters.Add(new SqlParameter("Username", username));
                        command.Parameters.Add(new SqlParameter("CharacterName", characterName));
                        command.Parameters.Add(new SqlParameter("CharacterStrength", strength));
                        command.Parameters.Add(new SqlParameter("CharacterAgility", agility));
                        command.Parameters.Add(new SqlParameter("CharacterIntelligence", intelligence));
                        command.Parameters.Add(new SqlParameter("CharacterLevel", level));
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                    Console.WriteLine("Character cannot be created, make sure the user exists");
                }
            }
        }

        static void TryFetchCharacterData(string p_username)
        {
            using (SqlConnection con = new SqlConnection(
       connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(
                        "SELECT * FROM USERDATABASE WHERE Username ='" + p_username + "'", con))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            string username = reader.GetString(0);
                            string characterName = reader.GetString(1);
                            string level = reader.GetString(2);
                            string strength = reader.GetString(3);
                            string agility = reader.GetString(4);
                            string intelligence = reader.GetString(5);
                            Console.WriteLine(username,characterName,level,strength,agility,intelligence);
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Could not fetch data");
                }
            }
        }
        static void Main(string[] args)
        {
            int userid;
            int strength=1;
            int agility=1;
            int intelligence=1;
            int level = 1;
            string characterName;
            string username;
            string emailaddress;
            string p_emailAddress;
            
            Console.WriteLine("Register UserID:");
            userid = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Register Username:");
            username = Console.ReadLine();
            Console.WriteLine("Register Email Address: ");
            emailaddress = Console.ReadLine();

            Program.AddUser(userid, username, emailaddress);
         
            Console.WriteLine("Input username to search for email:");
            p_emailAddress = Console.ReadLine();
            Program.TryDisplayEmail(p_emailAddress);

            Console.WriteLine("Register Character Name:");
            characterName = Console.ReadLine();

            Program.AddCharacter(username, characterName, level, strength, agility, intelligence);

            Console.WriteLine(username + "'s Character Info:");
            Console.WriteLine("Character Name:" + characterName);
            Console.WriteLine("Character Level:" + level);
            Console.WriteLine("Character Strength:" + strength);
            Console.WriteLine("Character Agility:" + agility);
            Console.WriteLine("Character Intelligence:" + intelligence);
            Console.ReadKey();
        }
    }
}
